﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using mstn;
using Jayrock.Json;
using Jayrock.Json.Conversion;

namespace kiosko2
{
    public class BalanceController : ApiController
    {
        [HttpGet]
        // GET api/<controller>
        public object Get()
        {
            string query="Select * from dbo.Numeros";
            return  JsonConvert.ExportToString(Utilidades.GetData(query));
        }

        [HttpGet]
        // GET api/<controller>/5
        public string Get(string phone)
        {
            return JsonConvert.ExportToString(Utilidades.GetData("Select * from dbo.Numeros where Numero='"+phone+"'")); ;
        }
        [HttpGet]
        [ActionName("GetDetails")]
        public string GetDetails(int num)
        {
            return JsonConvert.ExportToString(Utilidades.GetData("Select * from dbo.Facturas where ID_Numero='" + num + "' order by ID desc")); ;
        }
        [HttpGet]
        [ActionName("ValidaNumCedula")]
        public int ValidaNumCedula(string id, string doc)
        {
            return Utilidades.GetData("Select * from dbo.Clientes where Documento='" + doc + "' and ID='"+id+"' order by ID desc").Count;
        }
        [HttpGet]
        [ActionName("PagoTotal")]
        public int PagoTotal(string num, long tarjeta)
        {
            return 123456;
        }
        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }

    
    public class Utilidades { 
        //static cgdata cgdata = new mstn.cgdata();
        static mstn.cgdata cgdata = new mstn.cgdata();
        static private void conxEjecutar(string query)
        {
            if (cgdata.connection.State == ConnectionState.Closed)
                cgdata.connection.Open();
            cgdata.noquery(query);
            cgdata.connection.Close();
        }

        static public string ToJson(object obj)
        {
            return "";//JObject.FromObject(obj);
        }

        static public DataRowCollection GetData(string query)
        {
            DataSet ds = cgdata.query(query);
            return ds.Tables[0].Rows;
        }

        static public DataSet GetDataJson(string query)
        {
            DataSet ds = cgdata.query(query);
            return ds;//ds.Tables[0].Rows;
        }
    }
}